// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class SNAKEGAME_API Barrier
{
public:
	Barrier();
	~Barrier();
};
