// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameGameModeBase.generated.h"


UCLASS()
class SNAKEGAME_API ASnakeGameGameModeBase : public AGameModeBase

{
	GENERATED_BODY()

public:
	ASnakeGameGameModeBase();

	UPROPERTY(BlueprintReadWrite)
	float Points;
	
	UFUNCTION(BlueprintNativeEvent)
	void ChangePoints();
	void ChangePoints_Implementation();
};
